import matplotlib.pyplot as plot
#-*- coding:utf-8 -*-
input_values=[1,2,3,4,5]
squares=[1,4,9,16,25]
plot.plot(input_values,squares,linewidth=5)
#设置图表标题
plot.title("Square Numbers",fontsize=24)
#设置图表x轴
plot.xlabel("Value",fontsize=14)
#设置图表y轴
plot.ylabel("Square of Value",fontsize=14)
#设置刻度标记的大小
plot.tick_params(axis='both',labelsize=14)
plot.show()