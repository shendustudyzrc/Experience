#-*- coding:utf-8 -*-
 #数字的三次方被称为立方，绘制一个图形，显示前5个整数的立方值。
import matplotlib.pyplot as plot

input_values=[1,2,3,4,5]
squares=[1,8,27,64,125]
plot.plot(input_values,squares,linewidth=5)
#设置标题
plot.title("Cube Numbers",fontsize=24)
#设置x轴
plot.xlabel("Value",fontsize=14)
#设置y轴
plot.ylabel("Cube of Value",fontsize=14)
#设置x轴和y轴的边距
plot.tick_params(axis='both',labelsize=14)
plot.show()