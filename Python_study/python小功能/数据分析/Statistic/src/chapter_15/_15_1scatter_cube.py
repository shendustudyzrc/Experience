#-*- coding:utf-8 -*-
#绘制一个图形，显示前5000个整数的立方值,用散点图的方式表示
import matplotlib.pyplot as plot
x_values=list(range(1,5001))
y_values=[x**3 for x in x_values]
plot.scatter(x_values,y_values,edgecolors='none',s=40)
#设置标题
plot.title("Cube Numbers",fontsize=24)
#设置图表x轴
plot.xlabel("Value",fontsize=14)
#设置图表y轴
plot.ylabel("Cube of Value",fontsize=14)
#设置刻度标记的大小
plot.tick_params(axis='both',which='major',labelsize=14)
#描述x轴和y轴的坐标范围值
plot.axis([1,1100,0,1100000])
plot.show()