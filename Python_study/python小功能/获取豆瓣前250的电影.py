# -*- coding:utf-8 -*-  编码格式
import requests
from bs4 import BeautifulSoup
import re
for page in range(10):
    url='https://book.douban.com/top250?start='+str(page*25)+'&filter='
    print('----正在抓取第'+str(page+1)+'页的数据')
    html=requests.get(url)
    soup=BeautifulSoup(html.text,'html.parser')
    soup=str(soup)
    title=re.compile(r'<span class="title">(.*)</span>')#正则表达式 
    names=re.findall(title,soup)
    print(names)
    for name in names:
        print(name)


