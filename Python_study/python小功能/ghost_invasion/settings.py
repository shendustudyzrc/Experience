
class Settings():
    def __init__(self):
        #设置屏幕宽度
        self.screen_width=1200
        #设置屏幕高度
        self.screen_height=700
        #设置背景颜色
        self.background_color=(230,230,230)
        
        #设置捕手的数量
        self.hunters_count=3
        #设置炮弹参数
        
        #炮弹宽度
        self.bomp_width=3
        #炮弹高度
        self.bomp_height=20
        #炮弹颜色
        self.bomp_color=0,0,230
        #炮弹数量限制
        self.bomp_counts=10
        
        self.ghost_down_speed=5
        #定义该以怎样的速度来去加快游戏节奏
        self.speedup_scale=1.1
        #定义游戏难度上涨后的外星人点数提高速度
        self.score_scale=1.5
    #初始化随游戏而变化的设置
    def initial_dynamic_setting(self):
        #设置幽灵的移动像素
        self.ghost_moving_distance=1
        #炮弹移动距离
        self.bomp_moving_distance=1
        #设置捕手的移动像素
        self.hunter_moving_distance=1.5
        #ghost_direction为1表示向右移动，为-1时表示向左移动
        self.ghost_direction=1
        #记录一个幽灵值
        self.ghost_points=50
    def increase_speed(self):
        self.ghost_moving_distance*=self.speedup_scale
        self.bomp_moving_distance*=self.speedup_scale
        self.hunter_moving_distance*=self.speedup_scale
        self.ghost_points=int(self.ghost_points*self.score_scale)
        