import pygame
from pygame.sprite import Sprite
class Bomp(Sprite):
    def __init__(self,gi_setting,screen,hunter):
        super(Bomp,self).__init__()
        #获取屏幕这个参数
        self.screen=screen
        #创建一个子弹对象
        self.rect=pygame.Rect(0,0,gi_setting.bomp_width,gi_setting.bomp_height)
        #子弹出现的位置
        self.rect.centerx=hunter.rect.centerx
        self.rect.top=hunter.rect.top
        #存储用小数表示的子弹位置
        self.y=float(self.rect.y)
        #存储颜色和速度
        self.color=gi_setting.bomp_color
        self.moving_distance=gi_setting.bomp_moving_distance
    def update(self):
        #表示子弹位置
        self.y-=self.moving_distance
        #表示子弹的rect的位置
        self.rect.y=self.y
    def draw_bomp(self):
        pygame.draw.rect(self.screen, self.color, self.rect)
        