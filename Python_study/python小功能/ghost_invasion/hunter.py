import pygame
from pygame.sprite import Sprite
class Hunter(Sprite):
    def __init__(self,gi_setting,screen):
        super(Hunter, self).__init__()
    #设置捕手的位置
        self.screen=screen
    #引入设置对象
        self.gi_setting=gi_setting
    #1.加载捕手图片,获得图片和屏幕的外接矩形
        self.image=pygame.image.load("images/Hunt.bmp")
        self.rect=self.image.get_rect()
        self.screen_rect=screen.get_rect()
    #2.获取屏幕的x轴的中心位置
        self.rect.centerx=self.screen_rect.centerx
    #3.获取屏幕的y轴的中心位置
        self.rect.bottom=self.screen_rect.bottom
    #4.当捕手移动的时候，标志为移动状态,默认为false
        self.moving_up=False
        self.moving_down=False
        self.moving_right=False
        self.moving_left=False
    #5.在捕手的属性center中存储小数值，因为矩形块rect只能是整数值
        self.centerxValue=float(self.rect.centerx)
        self.centeryValue=float(self.rect.bottom)
    def deciplit(self):
    #1.绘制一个猎人
        self.screen.blit(self.image,self.rect)
    def update(self):
    #1.根据移动标志来变化位置
    #更新飞船的centerx值，而不是rect矩形块
    #限制捕手的移动，不能超出屏幕.这里的rect.right是相对于起点的
        if self.moving_up and self.centeryValue>self.screen_rect.top+20:
            self.centeryValue-=self.gi_setting.hunter_moving_distance
        if self.moving_down and self.centeryValue<self.gi_setting.screen_height:
            self.centeryValue+=self.gi_setting.hunter_moving_distance
        if self.moving_right and self.rect.right<self.screen_rect.right:
            self.centerxValue+=self.gi_setting.hunter_moving_distance
        if self.moving_left and self.rect.left>0:
            self.centerxValue-=self.gi_setting.hunter_moving_distance
    #2.更新捕手的位置
        self.rect.centerx=self.centerxValue
        self.rect.bottom=self.centeryValue
    def center_screen(self):
        self.centerxValue=self.gi_setting.screen_width/2
        self.centeryValue=self.gi_setting.screen_height