import sys
import pygame
from bomp import Bomp
from ghost import Ghost
from time import sleep
def check_event(gi_setting,screen,hunter,bomps,statistics,play_button,ghosts,sb):
    #响应键盘和鼠标事件处理
    for event in pygame.event.get():
    #1当用户点击右上角X号退出
        if event.type==pygame.QUIT:
            sys.exit()
    #2 当用户按下键盘时
        elif event.type==pygame.KEYDOWN:
            check_keydown_event(event, gi_setting,screen,hunter,bomps)
    #3 当用户松开键盘时
        elif event.type==pygame.KEYUP:
            check_keyup_event(event, hunter)    
    #4 鼠标按钮点击
        elif event.type==pygame.MOUSEBUTTONDOWN:
            mouse_x,mouse_y=pygame.mouse.get_pos()
            check_play_button(statistics, play_button, mouse_x, mouse_y, ghosts, bomps, gi_setting, hunter,sb,screen)
#检测玩家点击Play按钮的状态
def check_play_button(statistics,play_button,mouse_x,mouse_y,ghosts,bomps,gi_setting,hunter,sb,screen):
    button_click=play_button.rect.collidepoint(mouse_x,mouse_y)        
    if button_click and not statistics.game_active:
    #重置游戏设置：
        gi_setting.initial_dynamic_setting()
    #隐藏光标
        pygame.mouse.set_visible(False)
        
    #重置捕手个数信息
        statistics.reset_statistic()
        statistics.game_active=True
    #重置记分牌图像
        sb.prep_score()
        sb.prep_high_score()
        sb.prep_level()
    #展示剩余的捕手
        sb.prep_hunters()
    #清空幽灵数量
        ghosts.empty()
    #清空炸弹数量
        bomps.empty()
    #创建一群新的外星人
        create_ghosts(gi_setting, screen, hunter, ghosts)
    #捕手置中间位置
        hunter.center_screen()
def check_keydown_event(event,gi_setting,screen,hunter,bomps):
    #点击键盘右移
    if event.key==pygame.K_RIGHT:
        hunter.moving_right=True
    #点击键盘左移
    elif event.key==pygame.K_LEFT:
        hunter.moving_left=True
    #点击键盘上移
    elif event.key==pygame.K_UP:
        hunter.moving_up=True
    #点击键盘下移
    elif event.key==pygame.K_DOWN:
        hunter.moving_down=True
    elif event.key==pygame.K_SPACE:
        shit_bomps(bomps, gi_setting, screen, hunter)
def check_keyup_event(event,hunter):
    #松开右移键盘
    if event.key==pygame.K_RIGHT:
        hunter.moving_right=False
    #松开左移键盘
    elif event.key==pygame.K_LEFT:
        hunter.moving_left=False
    #松开上移键盘
    elif event.key==pygame.K_UP:
        hunter.moving_up=False
    #松开下移键盘
    elif event.key==pygame.K_DOWN:
        hunter.moving_down=False
def update_screen(gi_setting,screen,hunter,ghosts,bomps,statistics,play_button,sb):
    screen.fill(gi_setting.background_color)
    for bomp in bomps.sprites():
        bomp.draw_bomp()
    hunter.deciplit()
    ghosts.draw(screen)
    if not statistics.game_active:
        play_button.draw_button()
    sb.show_score()
    pygame.display.flip()  
   
def update_bomps(bomps,ghosts,gi_setting,screen,hunter,statistics,sb):
    #更新子弹位置
    bomps.update()
    #删除已消失的子弹
    for bomp in bomps.copy():
            if bomp.rect.bottom<=0:
                bomps.remove(bomp)  
    check_bomp_ghost_collision(bomps, ghosts, gi_setting, screen, hunter,statistics,sb)
#重构update_bomps中的碰撞情况
def check_bomp_ghost_collision(bomps,ghosts,gi_setting,screen,hunter,statistics,sb):
    #检查子弹和幽灵的碰撞情况
    collisions=pygame.sprite.groupcollide(bomps, ghosts, True,True)
    if collisions:
        for ghosts in collisions.values():
            statistics.score+=gi_setting.ghost_points
            sb.prep_score()
        check_high_score(statistics, sb)
    if len(ghosts)==0:
        bomps.empty()
        #提升幽灵们的速度
        gi_setting.increase_speed()
        #提高等级
        statistics.level+=1
        sb.prep_level()
        #创造新的幽灵们
        create_ghosts(gi_setting, screen, hunter, ghosts)  
def shit_bomps(bomps,gi_setting,screen,hunter):
    #限制子弹数量
    if len(bomps)<gi_setting.bomp_counts:
        new_bomp=Bomp(gi_setting,screen,hunter)
        bomps.add(new_bomp)
def create_ghosts(gi_setting,screen,hunter,ghosts):
    ghost=Ghost(gi_setting,screen)
    ghost_width=ghost.rect.width
    number_ghost_x=get_number_ghosts_x(gi_setting, ghost_width)
    rows_length=get_number_rows(gi_setting, hunter.rect.height, ghost.rect.height)
    for row_length in range(rows_length): 
        for ghost_count in range(number_ghost_x):
            create_ghost_script(gi_setting, screen, ghosts, ghost_count,row_length)        
def get_number_ghosts_x(gi_setting,ghost_width):
    #计算可剩余使用的行空间
    available_space_x=gi_setting.screen_width-4*ghost_width
    #计算一行摆放的幽灵个数
    number_ghost_x=int(available_space_x/(2*ghost_width))
    return number_ghost_x
def create_ghost_script(gi_setting,screen,ghosts,ghost_count,row_length):
    #创建第一行幽灵
    ghost=Ghost(gi_setting,screen)
    ghost.x=ghost.rect.width+2*ghost.rect.width*ghost_count
    #创建每一个幽灵的x轴坐标
    ghost.rect.x=ghost.x
    #创建每一个幽灵的y轴坐标
    ghost.rect.y=(ghost.rect.height-100)+2*ghost.rect.height*row_length
    ghosts.add(ghost)
#创建多行幽灵
def get_number_rows(gi_setting,hunter_height,ghost_height):
    #可剩余的空间，尽量保证离捕手远一点，不然很难通关呀
    available_space_y=(gi_setting.screen_height-hunter_height-5*ghost_height)
    rows_length=int(available_space_y/(2*ghost_height))
    return rows_length
#更新幽灵所在的位置
def update_ghost(gi_setting,ghosts,hunter,statistics,screen,bomps,sb):
    #如果有幽灵处于边缘，则改变它们的位置
    check_screen_edge(gi_setting, ghosts)
    ghosts.update()
    #检测幽灵和捕手的碰撞
    if pygame.sprite.spritecollideany(hunter, ghosts):
        ship_hit(gi_setting, statistics, screen, hunter, ghosts, bomps,sb)
    check_ghost_bottom(gi_setting, statistics, screen, hunter, ghosts, bomps,sb)
#检测幽灵是否到达边缘
def check_screen_edge(gi_setting,ghosts):
    for ghost in ghosts.sprites():
        if ghost.check_edges():
            change_ghost_direction(gi_setting,ghosts)
            break
#改变幽灵的移动方向
def change_ghost_direction(gi_setting,ghosts):
    #幽灵下移
    for ghost in ghosts.sprites():
        ghost.rect.y+=gi_setting.ghost_down_speed
    gi_setting.ghost_direction*=-1
#幽灵撞击到飞船的事件
def ship_hit(gi_setting,statistics,screen,hunter,ghosts,bomps,sb):
    #撞击到了，捕手数量减去1
    if statistics.hunters_leave>0:
        statistics.hunters_leave-=1
        #更新捕手数量
        sb.prep_hunters()
        #清空幽灵列表和子弹列表
        ghosts.empty()
        bomps.empty()
        #创建一群新的幽灵，并将捕手放在屏幕底端中央
        create_ghosts(gi_setting, screen, hunter, ghosts)
        hunter.center_screen()
        #暂停
        sleep(0.5)
    else:
        statistics.game_active=False
        pygame.mouse.set_visible(True)
#检测幽灵到达屏幕底端的事件
def check_ghost_bottom(gi_setting, statistics, screen, hunter, ghosts, bomps,sb):
    #检测是否有幽灵到达屏幕底端
    screen_rect=screen.get_rect()
    for ghost in ghosts.sprites():
        if ghost.rect.bottom>screen_rect.bottom:
            ship_hit(gi_setting, statistics, screen, hunter, ghosts, bomps,sb)
            break
#检查最高得分
def check_high_score(statistics,sb):
    #检查是否诞生了新的最高得分
    if statistics.score>statistics.high_score:
        statistics.high_score=statistics.score
        sb.prep_high_score()