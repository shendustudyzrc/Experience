import pygame
from pygame.sprite import Sprite
class Ghost(Sprite):
    def __init__(self,gi_setting,screen):
        super(Ghost,self).__init__()
        self.screen=screen
        self.gi_setting=gi_setting
        #创建一个幽灵对象
        self.image=pygame.image.load("images/ghost.bmp")
        self.rect=self.image.get_rect()
        #定义外星人的位置
        #以这个矩形块的宽和高来命名与原点的距离
        self.rect.x=self.rect.width
        self.rect.y=self.rect.height
        #存储外星人的准确位置
        self.x=float(self.rect.x)
    def deciplit(self):
        self.screen.blit(self.image,self.rect)
    #更新幽灵的位置
    def update(self):
        self.x+=(self.gi_setting.ghost_direction*self.gi_setting.ghost_moving_distance)
        #赋值操作
        self.rect.x=self.x
    #检测幽灵是否撞到边缘
    def check_edges(self):
        screen_rect=self.screen.get_rect()
        if self.rect.right>=screen_rect.right:
            return True
        elif self.rect.left<=0:
            return True  
        