class GameStatistic():
    def __init__(self,gi_setting):
        self.gi_setting=gi_setting
        self.reset_statistic()
        #让游戏一开始处于非活动状态
        self.game_active=False
        #保存最高得分
        self.high_score=0
        self.level=1
    def reset_statistic(self):
        self.hunters_leave=self.gi_setting.hunters_count
        self.score=0