import pygame.font
from pygame.sprite import Group
from hunter import Hunter
#-*- coding:utf-8 -*-  
class Scoreboard():
    #显示得分信息的类
    def __init__(self,gi_setting,screen,statistics):
        #初始化得分的属性
        self.screen=screen
        self.screen_rect=screen.get_rect()
        self.statistics=statistics
        self.gi_setting=gi_setting
        #显示得分信息时使用的设置
        self.text_color=(0,0,240)
        self.font=pygame.font.SysFont(None, 40)
        #准备初始化得到的图形
        self.prep_score()
        self.prep_high_score()
        self.prep_level()
        self.prep_hunters()
    def prep_score(self):
        #获取得分,变成千分位
        rounded_score=round(self.statistics.score,-1)
        score_str="{:,}".format(rounded_score)
        #将得分渲染成图像
        self.score_image=self.font.render(score_str,True,self.text_color)
        #摆放该得分图像得到的矩形块的位置
        self.score_rect=self.score_image.get_rect()
        self.score_rect.right=self.screen_rect.right-20
        self.score_rect.top=20
    #设置最高得分
    def prep_high_score(self):
        high_score=round(self.statistics.high_score, -1)
        high_score_str="{:,}".format(high_score)
        self.high_score_image=self.font.render(high_score_str,True,self.text_color,self.gi_setting.background_color)
        #将最高得分放在屏幕顶部中央
        self.high_score_rect=self.high_score_image.get_rect()
        self.high_score_rect.centerx=self.screen_rect.centerx
        self.high_score_rect.top=self.score_rect.top
    #在屏幕上展示飞船和得分
    def show_score(self):
        self.screen.blit(self.score_image,self.score_rect)
        self.screen.blit(self.high_score_image,self.high_score_rect)
        self.screen.blit(self.level_image,self.level_rect)
        #绘制飞船
        self.hunters.draw(self.screen)
    #展示玩家的水平
    def prep_level(self):
        #将等级转换为渲染的图像
        self.level_image=self.font.render(str(self.statistics.level),True,self.text_color,self.gi_setting.background_color)
        #设置等级所在的位置
        self.level_rect=self.level_image.get_rect()
        self.level_rect.right=self.score_rect.right
        self.level_rect.top=self.score_rect.bottom+10
    #展示飞船
    def prep_hunters(self):
        #"展示剩余捕手数量"
        self.hunters=Group()
        
        for hunter_number in range(self.statistics.hunters_leave):
            hunter=Hunter(self.gi_setting,self.screen)
            hunter.rect.x=10+hunter_number*hunter.rect.width
            hunter.rect.y=10
            self.hunters.add_internal(hunter)
     
        