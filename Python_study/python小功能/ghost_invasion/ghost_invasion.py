import pygame
from settings import Settings
from hunter import Hunter
import game_functions as game_f
from pygame.sprite import Group
from game_statistic import GameStatistic
from button import Button
from scoreboard import Scoreboard
def run_game():
    pygame.init()
    gi_setting=Settings()
    screen=pygame.display.set_mode((gi_setting.screen_width, gi_setting.screen_height))
    hunter=Hunter(gi_setting,screen)
    bomps=Group()
    pygame.display.set_caption("幽灵捕手")
    #创建一群幽灵
    ghosts=Group()
    game_f.create_ghosts(gi_setting,screen,hunter,ghosts)
    #创建一个按钮
    play_button=Button(gi_setting,screen,"Play")
    #创建用来存储游戏统计信息的实例
    statistics=GameStatistic(gi_setting)
    #创建得分牌
    sb=Scoreboard(gi_setting,screen,statistics)
    
    while True:
        game_f.check_event(gi_setting, screen, hunter, bomps, statistics, play_button, ghosts,sb)
        if statistics.game_active:
            hunter.update()
            #删除消失的子弹
            game_f.update_bomps(bomps, ghosts, gi_setting, screen, hunter, statistics, sb)
            game_f.update_ghost(gi_setting, ghosts, hunter, statistics, screen, bomps, sb)
        game_f.update_screen(gi_setting, screen, hunter,ghosts,bomps,statistics,play_button,sb)
run_game()