#!/usr/bin/env python
# encoding: utf-8
#导入requests包
import requests
#输入链接地址
link="http://www.santostang.com"
#输入浏览器头部，避免被网站识别
headers={'User-Agent':'Mozilla/5.0(Windows;U;Windows NT 6.1;en_US;rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6'}
#使用requests这个包中的get()方法
r=requests.get(link,headers=headers)
#打印文本
print(r.text)
