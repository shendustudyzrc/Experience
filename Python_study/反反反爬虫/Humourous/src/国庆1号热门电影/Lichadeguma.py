from urllib import request
from bs4 import BeautifulSoup as bs
import re
import jieba
import matplotlib.pyplot as plt
import matplotlib
from wordcloud import WordCloud
import pandas as pd
import numpy
matplotlib.rcParams['figure.figsize'] = (10.0, 5.0)
#获取数据源
def get_page(url):
    response=request.urlopen(url)
    html_data=response.read().decode('utf-8')
    soup=bs(html_data,'html.parser')
    return soup
#处理数据：
def get_information(soup):
    nowplaying_movie=soup.find_all('div', id='nowplaying')
    print(nowplaying_movie[0])
    nowplaying_movie_list = nowplaying_movie[0].find_all('li', class_='list-item')
    nowplaying_list = [] 
    for item in nowplaying_movie_list:
        nowplaying_dict = {}
        nowplaying_dict['id'] = item['data-subject']   
        for tag_img_item in item.find_all('img'):
            nowplaying_dict['name'] = tag_img_item['alt']
            nowplaying_list.append(nowplaying_dict)
    #获取李茶的姑妈这部电影
    requrl = 'https://movie.douban.com/subject/' + nowplaying_list[0]['id'] + '/comments' +'?' +'start=0' + '&limit=20' 
    resp = request.urlopen(requrl) 
    html_data = resp.read().decode('utf-8') 
    soup_comments= bs(html_data, 'html.parser') 
    comment_div_lits = soup_comments.find_all('div', class_='comment')
    eachCommentList = []; 
    for item in comment_div_lits:   
        eachCommentList.append(item.find_all('span',attrs={'class':"short"}))
    comments=""
    for k in range(len(eachCommentList)):
        comments=comments+(str(eachCommentList[k])).strip()
    pattern = re.compile(r'[\u4e00-\u9fa5]+')
    filterdata = re.findall(pattern, comments)
    cleaned_comments = ''.join(filterdata)
    return cleaned_comments
#生成词云图
def get_wordCloud_Picture(cleaned_comments):
    segment = jieba.lcut(cleaned_comments)
    words_df=pd.DataFrame({'segment':segment})
    #去掉一些分词
    stopwords=pd.read_csv("Z:\Experience\stopwords.txt",index_col=False,quoting=3,sep="\t",names=['stopword'], encoding='utf-8')
    words_df=words_df[~words_df.segment.isin(stopwords.stopword)]
    words_stat=words_df.groupby(by=['segment'])['segment'].agg(numpy.size)
    words_stat=words_stat.to_frame()
    words_stat.columns=['count']
    words_stat=words_stat.reset_index().sort_values(by=["count"],ascending=False)
    # print(words_stat)
    wordcloud=WordCloud(font_path="Z:\Experience\simheittf\simhei.ttf",background_color="white",max_font_size=80) 
    word_frequence = {x[0]:x[1] for x in words_stat.head(1000).values}
    wordcloud=wordcloud.fit_words(word_frequence)
    plt.imshow(wordcloud)
    plt.show()
#主函数进行函数调用
def main():
    url="https://movie.douban.com/cinema/nowplaying/beijing/"
    soup=get_page(url)
    cleaned_comments=get_information(soup)
    get_wordCloud_Picture(cleaned_comments)
#执行主函数
main()
