#coding=UTF-8
import requests
import MySQLdb
import re
import json
from pymongo import MongoClient 
import jieba#用于切词
from wordcloud import WordCloud # 用于生成词云
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup
#存储到数据库中
def write_mysql():
    conn=MySQLdb.connect(host='localhost',user='root',password='123456',db='scrapy',charset="utf8")
    cur=conn.cursor()
    return cur
#MongoClient是Mongo数据库
#     client=MongoClient('localhost',27017)
#     db=client.humor_database
#     collection=db.blog1
#     return collection
#获取页面
def get_page(link):
    headers={'User-Agent':'Mozilla/5.0(Windows;U;Windows NT 6.1;en_US;rv:1.9.1.6)Gecko/20091201 Firefox/3.5.6'}
    r=requests.get(link,headers=headers)
    soup=BeautifulSoup(r.text,"html.parser")
    return soup
#获取数据
def get_information(soup):
#     information=re.findall('<div class="content">(.*?)</div>',html,re.S)
    content_list=soup.find_all('div',class_="content")
#     print(content_list)
    cur=write_mysql()
#     allComment=""
    for content in content_list:
        title=content.span.text.strip()
        print(title)
#         allComment+=str(title).strip()
        write_information(title)
#         content={"content":title}
#         cur.insert_one(content)   
        content=title
        #待解决
#         cur.execute("insert into humous(content) values(%s)",(content))
#     generate_wordcloud(allComment)
    return content_list
#存储数据
def write_information(information):
    with open('humor.txt','a+',encoding='utf-8') as f:
        f.write(json.dumps(information,ensure_ascii=False)+'\n')
#生成词云图：
def generate_wordcloud(allComment):
    pattern = re.compile(r'[\u4e00-\u9fa5]+')
    finalComment = ''.join(re.findall(pattern, allComment))
    segment = jieba.lcut(finalComment)
    words_df = pd.DataFrame({'segment': segment})
    print(words_df)
    '''remove useless words'''
    stopwords = pd.read_csv("Z:\Experience\stopwords.txt", index_col=False, quoting=3, sep="\t",
    names=['stopword'], encoding='utf-8')
    words_df = words_df[~words_df.segment.isin(stopwords.stopword)]
    #获得词云频率
    words_fre = words_df.groupby(by='segment')['segment'].agg({'count': np.size})
    words_fre = words_fre.reset_index().sort_values(by='count', ascending=False)
    wordcloud = WordCloud(font_path='Z:\Experience\simheittf\simhei.ttf', background_color='white', max_font_size=80)
    word_fre_dic = {x[0]: x[1] for x in words_fre.values}
    wordcloud = wordcloud.fit_words(word_fre_dic)
    plt.imshow(wordcloud)
    plt.show()
def main(offset):
    #获取7页的数据
    if offset==0:
        link="https://www.qiushibaike.com"
    else:
        link="https://www.qiushibaike.com/8hr/page/"+str(offset)+"/"
    print(link)
    soup=get_page(link)
    content_list=get_information(soup)
    allComment=""
    for content in content_list:
        title=content.span.text.strip()
        print(title)
        allComment+=str(title).strip()
    generate_wordcloud(allComment)
if __name__=='__main__':
    for i in range(13):
        main(offset=i)