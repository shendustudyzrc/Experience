#获取猫眼电影排行
#encoding:utf-8
import requests
import re
import time
import json
from idlelib.iomenu import encoding
import pygal
from pygal.style import LightColorizedStyle as LCS,LightenStyle as LS
import matplotlib.pyplot
#获取一个页面
def get_one_page(url):
    headers={'User-Agent':'Mozilla/5.0(Windows;U;Windows NT 6.1;en_US;rv:1.9.1.6)Gecko/20091201 Firefox/3.5.6'}
    response=requests.get(url,headers=headers)
    if response.status_code==200:
        return response.text
    else:
        return None
#获取电影名称,正则表达式提取
def get_movie_information(html):
    pattern = re.compile('<dd>.*?board-index.*?>(\d+)</i>.*?<p class="name"><a .*?>(.*?)</a></p>.*?<p class="star">(.*?)</p>.*?<p class="releasetime">(.*?)</p>',re.S)
    items=re.findall(pattern, html)
    for item in items:
        yield{
            'index:':item[0],
            'movie_title:':item[1],
            'movie_actor:':item[2],
            'movie_show_time:':item[3]
        }
#可视化
def show(items):
    movie_titles,movie_show_times=[],[]
    for item in items:
        movie_titles.append(str(item['movie_title:']))
#         s=str(item['movie_show_time:']).replace("上映时间：", "").replace("-", "")
#         if int(s)<20181007 and int(s)>20181001:
        movie_show_times.append(int(item['index:']))
#可视化，存储到svg文件中
    my_style=LS('#333366',base_style=LCS)
    chart=pygal.Bar(style=my_style,x_label_rotation=45,show_legend=False)
    chart.title='猫眼最受期待的电影'
    chart.x_labels=movie_titles
    chart.add('', movie_show_times)
    chart.render_to_file("maoyan_movie1.svg")
#存储到csv文件中：
def write_to_file(movie):
    with open("maoyan_table1.csv",'a',encoding='utf-8') as f:
        f.write(json.dumps(movie,ensure_ascii=False)+'\n')
#执行主函数
def main(offset):
    url='http://maoyan.com/board/6?offset='+str(offset)
    html=get_one_page(url)
    for item in get_movie_information(html):
        #查看控制台输出的数据
        if int(item['index:'])<=10 and int(str(item['movie_show_time:']).replace("上映时间：", "").replace("-", ""))<20181007:
            print(item['index:'],item['movie_title:'],item['movie_show_time:'])
        write_to_file(item)     
        
    html_svg=get_one_page('http://maoyan.com/board/6?offset=0')   
    show(get_movie_information(html_svg))
    
if __name__=='__main__':
    for i in range(5):
        main(offset=i*10)
        time.sleep(1)