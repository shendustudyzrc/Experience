import requests
import re
import jieba#用于切词
from wordcloud import WordCloud # 用于生成词云
import matplotlib.pyplot as plt
import matplotlib
import json
import pandas as pd
import numpy as np
#获取数据源
def get_one_page(url):
    headers={'User-Agent':'Mozilla/5.0(Windows;U;Windows NT 6.1;en_US;rv:1.9.1.6)Gecko/20091201 Firefox/3.5.6'}
    
    response=requests.get(url,headers=headers)
    return response.text
#获取需要的信息
def get_information(html):
    pattern=re.compile('<div class="comment-content">(.*?)</div>',re.S)
    items=re.findall(pattern,html)
    allComment=''
    for item in items:
        print(item+'\n')
        write_information(item)
        allComment+=str(item).strip()
    generate_wordcloud(allComment)
#存储信息：
def write_information(contents):
    with open("zhaodaoni.txt",'a+',encoding='utf-8') as f:
        f.write(contents)
    
#生成词云图：
def generate_wordcloud(allComment):
    pattern = re.compile(r'[\u4e00-\u9fa5]+')
    finalComment = ''.join(re.findall(pattern, allComment))
    segment = jieba.lcut(finalComment)
    words_df = pd.DataFrame({'segment': segment})
    print(words_df)
    '''remove useless words'''
    stopwords = pd.read_csv("Z:\Experience\stopwords.txt", index_col=False, quoting=3, sep="\t",
    names=['stopword'], encoding='utf-8')
    words_df = words_df[~words_df.segment.isin(stopwords.stopword)]
    #获得词云频率
    words_fre = words_df.groupby(by='segment')['segment'].agg({'count': np.size})
    words_fre = words_fre.reset_index().sort_values(by='count', ascending=False)
    wordcloud = WordCloud(font_path='Z:\Experience\simheittf\simhei.ttf', background_color='white', max_font_size=80)
    word_fre_dic = {x[0]: x[1] for x in words_fre.values}
    wordcloud = wordcloud.fit_words(word_fre_dic)
    plt.imshow(wordcloud)
    plt.show()
#执行主函数
def main():
    url="http://maoyan.com/films/1198828"
    html=get_one_page(url)
    get_information(html)
main()