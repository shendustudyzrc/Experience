    import warnings  # 防止出现future warning
    warnings.filterwarnings("ignore")
    from urllib import request # 用于爬取网页
    from bs4 import BeautifulSoup as bs # 用于解析网页
    import re
    import pandas as pd
    import numpy as np
    import jieba # 用于切词
    from wordcloud import WordCloud # 用于生成词云
    import matplotlib.pyplot as plt
    import matplotlib
    url = 'https://movie.douban.com/nowplaying/shanghai/'
    resp = request.urlopen(url)
    html_data = resp.read().decode('utf-8') # 防止乱码
     
    soup = bs(html_data, 'html.parser') # 解析
    nowplaying = soup.find_all('div', id='nowplaying') # 网页中id为nowplaying是现在正在上映的电影。
    nowplaying_list = nowplaying[0].find_all('li', class_='list-item') # 寻找所有上映电影相关信息
    '''get movie list''' 
    movie_list = [] # 获取电影id和电影名
    for item in nowplaying_list:
    movie_dic = {}
    movie_dic['id'] = item['id']
    movie_dic['name'] = item['data-title']
    movie_list.append(movie_dic)
    
    '''first is 'zuihaodewomen', get comments'''
    url_comment = 'https://movie.douban.com/subject/' + movie_list[1]['id'] + '/comments?start=' + '0' + '&limit=20'
    resp = request.urlopen(url_comment)
    html_comment = resp.read().decode('utf-8')
    soup_comment = bs(html_comment, 'html.parser')
    comment_list = soup_comment.find_all('div', class_='comment')
    
    '''get comment list'''
    comments = []
    for item in comment_list:
    comments.append(item.find_all('span',attrs={'class':"short"}))
    print(comments)
    
    '''clean comments'''
    allComment = ''
    for item in range(len(comments)):
    allComment=allComment+(str(comments[item])).strip()
    
    pattern = re.compile(r'[\u4e00-\u9fa5]+')
    finalComment = ''.join(re.findall(pattern, allComment))
     
    segment = jieba.lcut(finalComment)
    words_df = pd.DataFrame({'segment': segment})
    print(words_df)
    '''remove useless words'''
    stopwords = pd.read_csv("Z:\Experience\stopwords.txt", index_col=False, quoting=3, sep="\t",
    names=['stopword'], encoding='utf-8')
    words_df = words_df[~words_df.segment.isin(stopwords.stopword)]
     
    '''get words frequency'''
    words_fre = words_df.groupby(by='segment')['segment'].agg({'count': np.size})
    words_fre = words_fre.reset_index().sort_values(by='count', ascending=False)
    '''use wordcloud'''
    matplotlib.rcParams['figure.figsize'] = [10.0, 5.0]
    wordcloud = WordCloud(font_path='Z:\Experience\simheittf\simhei.ttf', background_color='white', max_font_size=80)
    word_fre_dic = {x[0]: x[1] for x in words_fre.values}
    wordcloud = wordcloud.fit_words(word_fre_dic)
    plt.imshow(wordcloud)
    plt.show()
    

![](https://i.imgur.com/seb1yt9.png)